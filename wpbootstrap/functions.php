<?php 

function wpbootstrap_scripts_with_jquery()
{
	//wp_register_script( 'custom-script', get_template_directory_uri() . '/bootstrap/js/bootstrap.js', array( 'jquery' ) );
	wp_register_script( 'boostrap-script', '//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js', array( 'jquery' ) );
	wp_register_script( 'hu-script', get_template_directory_uri() . '/bootstrap/js/hu.js', array( 'jquery' ) );

	wp_enqueue_script( 'boostrap-script' );
	wp_enqueue_script( 'hu-script' );

}


add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );


add_action( 'init', 'register_header_menu' );


add_action( 'widgets_init', 'my_register_sidebars' );


function my_register_sidebars() {
	register_sidebar(array(
	'name' => __( 'Default Right Sidebar' ),
	'id' => 'right-sidebar',
	'description' => __( 'Widgets in this area will be shown on the right-hand side.' ),
	'before_title' => '<h4>',
	'after_title' => '</h4>',
	'before_widget' => ' ',
	'after_widget'  => ' ',
	));

	register_sidebar(array(
	'name' => __( 'Home Right Sidebar' ),
	'id' => 'home-sidebar',
	'description' => __( 'Widgets in this area will be shown on the homepage on the right' ),
	'before_title' => '<h4>',
	'after_title' => '</h4>',
	'before_widget' => ' ',
	'after_widget'  => ' ',
	));



}

function custom_excerpt_length( $length ) {
	return 150;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function sendInfo(){


	$name = $_POST['name'];
	$email = $_POST['email'];
	$organization = $_POST['organization'];
	$message = $_POST['message'];
	trigger_error( $message );
	$to = get_option('admin_email');
	$subject = 'Harlem United website contact form';
	$body = "Name: $name  \n\nEmail: $email \n\nOrganization: $organization \n\nMessage: $message";
	$headers = 'From: '.$name.' <'.$to.'>' . "\r\n" . 'Reply-To: ' . $email;
	if( wp_mail( $to, $subject, $body, $headers ) ) {
		// the message was sent...
		echo 'Message sent!';
	} else {
		// the message was not sent...
		echo 'The message was not sent!';
	};
	die();

}
add_action('wp_ajax_sendInfo', 'sendInfo');
add_action('wp_ajax_nopriv_sendInfo', 'sendInfo');

function register_header_menu() {
	register_nav_menu( 'main-menu', __( 'Main Menu' ) );
}


function get_homepage_story($category_name) {
	$args = array(
			'post_type' => 'post',
			'category_name' => $category_name,
			'tag' => 'home',
			'posts_per_page' => 1
	);
	$query = new WP_Query($args);
	while ( $query->have_posts() )
	{
		$query->the_post();
		echo  the_title( '<h4>', '</h4>', FALSE ) ;
		echo  the_excerpt();
	}
	wp_reset_query();
}


function get_faqs(){

	$id = get_cat_ID("FAQ's");
	$categories=  get_categories('child_of='.$id);
	foreach ($categories as $category) {
		$cat_name =  $category->name;
		echo '<h4>'.$cat_name . '</h4>';

		get_posts_per_page($category->category_nicename);
	}


}

function get_posts_per_page($category_name, $useSnippet=false) {
	$args = array(
			'post_type' => 'post',
			'category_name' => $category_name,
			'nopaging' => true
	);

	$query = new WP_Query($args);
	echo  '<div class="accordion" id="page-accordion">';

	while ( $query->have_posts() )
	{
		$query->the_post();

		echo '<div class="accordion-group">';
		 
		echo '<div class="accordion-heading">';
		echo '<a class="accordion-toggle" data-toggle="collapse" data-parent="#page-accordion" href=' . '#collapse' . $query->post->ID . '>';
		echo '<i class="icon-chevron-right"></i>';
		echo the_title( '', '', FALSE );
		echo '</a>';
		echo '</div>';

		$contentClass = '<div id=' . 'collapse'. $query->post->ID . ' class="accordion-body collapse">';
		echo $contentClass;
		echo '<div class="accordion-inner">';
		if ($useSnippet) { echo  the_excerpt(); } else {echo  the_content();}
		echo '</div>';
		echo '</div>';
		//close accordion-group
		echo '</div>';



	}
	wp_reset_query();
	//close div page-accordion
	echo '</div>';
}

function get_snippet_posts_per_page($category_name){
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args = array(
			'post_type' => 'post',
			'category_name' => $category_name,
			'posts_per_page' => 5,
			'paged' => $paged
	);
	$query = new WP_Query($args);
	while ( $query->have_posts() )
	{
		$query->the_post();
	    echo '<h4>';
		printf(	'<a href="%s" title="%s">%s</a>', get_permalink(), the_title_attribute( 'echo=0' ), get_the_title() );
		echo '</h4>';
		echo  the_excerpt();
	    echo  '<br/>';
	 
	}
	
            $pag_args = array(
                'format'  => '?paged=%#%',
                'current' => $paged,
                'total'   => $query->max_num_pages
            );
            // reverse pagaination naviation does not work with custom uri structures 
            echo paginate_links( $pag_args );
            wp_reset_query();
}


function get_hu_header_image(){
	$images =
	array(
			"services"    => "overview_header.jpg",
			"careers"  => "career_header.jpg",
			"policy"  => "policy_header.jpg",
			"community_support" => "community_support_header.jpg",
			"locations" => "locations_header.jpg",
			"news" => "news_header.jpg",
			"housing" => "housing_header.jpg",
			"health_care" => "healthcare_header.jpg",
			"featured" => "featured_header.jpg",
			"bmi" => "bmi_header.jpg",
			"contact" => "overview_header.jpg",
			"events" => "events_header.jpg",
			"press_releases" => "press_releases_header.jpg",
			"get_help" => "get_help_header.jpg",
			"achievement & awards"	=> "achievement_awards_header.jpg",
			"board & management" => "board_management_header.jpg",
			"who we are" => "who_we_are_header.jpg",
			"mission & history" => "mission_history_header.jpg",
			"volunteer / intern" => "volunteer_intern_header.jpg",
			"art gallery" => "art_gallery_header.jpg",
			"video gallery" => "video_gallery_header.jpg",
			"annual report" =>	"annual_report.jpg",
			"faqs" =>	"faqs_header.jpg"

	);




	if(is_page()) {
		$pgname = strtolower(basename( get_page_template(),".php"));
		if($pgname == "page") {

			$pgname = strtolower( html_entity_decode(the_title('', '', FALSE)));
			//$pgname = strtolower( the_title('', '', FALSE));

		}

		$headerImg =  $images[trim($pgname)];

	}
	elseif(is_single()) {
		// TODO: Do we need a header for individual posts?
		//$categories = get_the_category();
		//echo $categories[0]->cat_name;

	}

    $img ='';
	if(!empty($headerImg)) {
		$img = '<img src="';
		$img .= '/images/' ;
		$img .= $headerImg ;
		$img .= '"/>';
	}
	echo $img;
}

function new_excerpt_more($more) {
	return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Read More&#0133;</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );


class Bootstrap_Walker_Nav_Menu extends Walker_Nav_Menu {


	function start_lvl( &$output, $depth ) {

		$indent = str_repeat( "\t", $depth );
		$output	   .= "\n$indent<ul class=\"dropdown-menu\">\n";

	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$li_attributes = '';
		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = ($args->has_children) ? 'dropdown' : '';
		//BC : Commenting b/c don't need current selected logic
		//$classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
		$classes[] = 'menu-item-' . $item->ID;


		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';
		//BC: Changing id attribute to use text value instead of menu-id
		//$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. str_replace(' ', '',strtolower($item->title) ) , $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		$attributes .= ($args->has_children) 	    ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= ($args->has_children) ? ' <b class="caret black-caret"></b></a>' : '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

		if ( !$element )
			return;

		$id_field = $this->db_fields['id'];

		//display this element
		if ( is_array( $args[0] ) )
			$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
		else if ( is_object( $args[0] ) )
			$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
		$cb_args = array_merge( array(&$output, $element, $depth), $args);
		call_user_func_array(array(&$this, 'start_el'), $cb_args);

		$id = $element->$id_field;

		// descend only when the depth is right and there are childrens for this element
		if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

			foreach( $children_elements[ $id ] as $child ){

				if ( !isset($newlevel) ) {
					$newlevel = true;
					//start the child delimiter
					$cb_args = array_merge( array(&$output, $depth), $args);
					call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
				}
				$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
			}
			unset( $children_elements[ $id ] );
		}

		if ( isset($newlevel) && $newlevel ){
			//end the child delimiter
			$cb_args = array_merge( array(&$output, $depth), $args);
			call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
		}

		//end this element
		$cb_args = array_merge( array(&$output, $element, $depth), $args);
		call_user_func_array(array(&$this, 'end_el'), $cb_args);

	}

}


?>