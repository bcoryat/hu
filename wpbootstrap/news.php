<?php
/*
 Template Name: news
*/
?>

<?php get_header(); ?>
<div id="main-content" class="container">
	<div class="row-fluid">
		<div class="span8">
			<h2>News</h2>
			<?php get_posts_per_page('News', true)  ?>
		</div>

		<div class="span4">
			<?php dynamic_sidebar( 'Default Right Sidebar' ); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
