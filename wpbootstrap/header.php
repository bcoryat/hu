<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title><?php wp_title('|',1,'right'); ?> <?php bloginfo('name'); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
 <!--[if lt IE 8]>
     <link href="//netdna.bootstrapcdn.com/font-awesome/2.0/css/font-awesome-ie7.css" rel="stylesheet">
   <![endif]-->
<!-- Le styles -->
<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<?php wp_enqueue_script("jquery"); ?>
<?php wp_head(); ?>
</head>
<body>

	<div class="navbar  navbar-fixed-top">
		<?php 
		// Fix menu overlap bug..
		if ( is_admin_bar_showing() ) echo '<div style="min-height: 28px;"></div>';
		?>
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse"
					data-target=".nav-collapse"> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <span class="icon-bar"></span>
				</a> <a class="brand" href="<?php echo site_url(); ?>"><img
					src='/images/logo.png'> </a>
				<div class="nav-collapse collapse">
					<?php	
					$args = array(
					'depth'		 => 2,
					'container'	 => false,
					'menu_class' => 'nav  pull-right',
					'walker'	 => new Bootstrap_Walker_Nav_Menu()
				);
				wp_nav_menu($args);
				?>
				</div>
				<!--/.nav-collapse -->

			</div>
		</div>
	</div>


	<div id="headerimg" class="container clear-top">
    	<div class="clearfix row-fluid" id="hu-header">
      
			<div class="span12"> 
			<?php
                 if ( is_home() ) {                    
	               easingsliderpro( 1 );
                 } else {
                     
                     get_hu_header_image();
                 }
             ?>
			</div>
		</div>
	</div>