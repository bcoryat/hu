<?php
/*
 Template Name: Featured
*/
?>


<?php get_header(); ?>
<div id="main-content" class="container">
	<div class="row-fluid">
		<div class="span8">
			<h2>Featured</h2>
			<?php get_posts_per_page('Featured', true)  ?>
		</div>
       
		<div class="span4">
			<?php dynamic_sidebar( 'Default Right Sidebar' ); ?>
		</div>
		
	</div>
</div>
<?php get_footer(); ?>
