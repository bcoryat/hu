
<?php get_header(); ?>
<div id="main-content" class="container">
	<div class="row-fluid">
		<div class="span8">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php the_title( '<h2>', '</h2>' ); ?>
			<?php the_content(); ?>

			<?php endwhile; else: ?>
			<p>
				<?php _e('Sorry, this page does not exist.'); ?>
			</p>
			<?php endif; ?>

		</div>
		<div class="span4">
			<?php dynamic_sidebar( 'Default Right Sidebar' ); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>

