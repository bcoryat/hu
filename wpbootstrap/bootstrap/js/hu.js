var j = jQuery;

j(document).on('click.collapse.data-api', '.accordion-toggle', function(e) {
	var icon = j(this).find('i');
	var others =j('i').not(icon);
    others.removeClass('icon-chevron-down').addClass('icon-chevron-right');	
	if (icon.hasClass('icon-chevron-right')) {
		icon.removeClass('icon-chevron-right').addClass('icon-chevron-down');
	} else {
		icon.removeClass('icon-chevron-down').addClass('icon-chevron-right')
	}
});