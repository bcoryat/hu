<?php
/*
 Template Name: Get Help
*/
?>




<?php get_header(); ?>
<div id="main-content" class="container">
	<div class="row-fluid">
		<div class="span8">
			<h2>Get Help</h2>
			<?php  while ( have_posts() ) : the_post();
			   the_content();
            endwhile;  ?>
			<?php  get_posts_per_page('Get Help')  ?>
		</div>

		<div class="span4">
			<?php dynamic_sidebar( 'Default Right Sidebar' ); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
