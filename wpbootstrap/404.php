
<?php get_header(); ?>
<div id="main-content" class="container">
	<div class="row-fluid">
		<div class="span12">


			<div class="hero-unit center">
				<h1>
					<small>404</small> Page Not Found 
				</h1>
				<br />
				<p>
					The page you requested could not be found, either contact your
					webmaster or try again. 
					<br/> Use your browsers <b>Back</b> button to
					navigate to the page you have prevously come from
				</p>

			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>

