<?php get_header(); ?>
<div id="main-content" class="container">
	<div class="row-fluid">
		<div class="span4">
			<h3>News and Events</h3>
			<?php  get_homepage_story('News')  ?>
		</div>
		<div class="span4">
			<h3>Policy</h3>
			<?php  get_homepage_story('Policy')  ?>
		</div>
		<div class="span3">
			<?php dynamic_sidebar( 'Home Right Sidebar' ); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>