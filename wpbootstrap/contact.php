<?php
/*
 Template Name: Contact
*/
?>

<?php get_header(); ?>
<div id="main-content" class="container">

	<div class="row-fluid">
		<div class="span9">
			<h2>Contact</h2>
			<?php  while ( have_posts() ) : the_post();
			the_content();
            endwhile;  ?>
			<?php  get_posts_per_page('Contact')  ?>
		</div>


		<div class="span3">
			<h4>Send a Message</h4>
			<form type="post" action="" id="contact_form">
				<fieldset>
					<div class="control-group">
						<div class="controls">
							<label class="control-label">Name</label> 
							<input id="name" name="name" type="text" class="input-large"> 
							<label class="control-label">Email Address</label> 
							<input id="email" name="email" type="text" class="input-large"> 
							<label class="control-label">Organization</label> 
							<input id="organization" name="organization" type="text" class="input-large"> 
							<label class="control-label">Message</label>
							<textarea id="message" name="message" required="" rows="10"></textarea>
							<input type="hidden" name="action" value="sendInfo"/>
							
						</div>
					</div>
				    <div class="control-group">
							<button  type="submit" id="send" name="send" class="btn btn-warning">Send</button>
					</div>	
					<div id="msg" class="text-success"></div>			
				</fieldset>
			</form>

		</div>


	</div>


</div>

 <script type="text/javascript">
 jQuery('#contact_form').submit(ajaxSubmit);
                                               
                                                function ajaxSubmit(){
                                                       
                                                        var contactForm = jQuery(this).serialize();
                                                       
                                                        jQuery.ajax({
                                                                type:"POST",                                                                                                                             
                                                                url: '<?php echo admin_url('admin-ajax.php');?>',  
                                                                data: contactForm,
                                                                success:function(data){
                                                                     jQuery("#msg").html(data);
                                                                },
                                                                error: function(errorThrown){
                                                                 alert(errorThrown);
                                                               }  
                                                        });
                                                       
                                                        return false;
                                                }
                                        </script>
                                       
 
 
<?php get_footer(); ?>